## Tensorflow test in CMSSW

##### Installation

```bash
source /cvmfs/cms.cern.ch/cmsset_default.sh

export SCRAM_ARCH="slc6_amd64_gcc530"
export CMSSW_VERSION="CMSSW_8_0_26_patch2"

cmsrel $CMSSW_VERSION
cd $CMSSW_VERSION/src
cmsenv

git clone https://gitlab.cern.ch/mrieger/TensorflowTest.git
./TensorflowTest/setup.sh

scram b -j

run_tftest tf2
```


##### Usage of `run_tftest`

```bash
> run_tftest [mode]
```

`mode`s:

- `tf` (default): Tests if tensorflow can be imported.
- `np`: Tests if numpy can be imported.
- `tf2`: Imports tensorflow, setups up some tensors and a session and checks an evaluation call.
