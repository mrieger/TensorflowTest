/*
 * Simple test of tensorflow and numpy functionality via the Python C API.
 *
 * Usage:
 *   > run_tftest [mode]
 *
 * Valid modes are "tf" (the default) and "np" which perform basic tests via tensorflow and numpy,
 * respectively, and "tf2" which performs a real tensorflow session evaluation test.
 *
 * Marcel Rieger.
 */

#include <iostream>
#include "Python.h"

static std::string pyScript = "\
import os, sys\n\
\n\
def insert_path(path):\n\
    path = os.path.expandvars(os.path.expanduser(path))\n\
    sys.path.append(path)\n\
\n\
def test_tf():\n\
    import tensorflow as tf\n\
    return 1\n\
\n\
def test_tf2():\n\
    import tensorflow as tf\n\
    x_ = tf.placeholder(tf.float32, [None, 10])\n\
    W = tf.Variable(tf.ones([10, 1]))\n\
    b = tf.Variable(tf.ones([1]))\n\
    y = tf.matmul(x_, W) + b\n\
    sess = tf.Session()\n\
    sess.run(tf.global_variables_initializer())\n\
    return sess.run(y, feed_dict={x_: [range(10)]})[0][0]\n\
\n\
def test_np():\n\
    import numpy as np\n\
    return 1\n\
";

void pyInitialize()
{
    PyEval_InitThreads();
    Py_Initialize();
}

void pyFinalize()
{
    Py_Finalize();
}

void pyExcept(PyObject* pyObj, const std::string& msg)
{
    // when a python object is NULL, an error occured
    if (pyObj == NULL)
    {
        // check if there is a python error on the stack
        if (PyErr_Occurred() != NULL)
        {
            PyErr_PrintEx(0);
        }
        throw std::runtime_error("a python error occured: " + msg);
    }
}

PyObject* pyInitContext()
{
    // create the main module
    PyObject* pyMainModule = PyImport_AddModule("__main__");

    // copy the global dict to create a new reference rather than borrowing one
    PyObject* pyMainDict = PyModule_GetDict(pyMainModule);
    PyObject* pyContext = PyDict_Copy(pyMainDict);

    // run the static script in that context
    PyObject* pyResult = PyRun_String(pyScript.c_str(), Py_file_input, pyContext, pyContext);
    pyExcept(pyResult, "error during script initialization");

    // decrease borrowed references
    Py_DECREF(pyResult);
    Py_DECREF(pyMainDict);

    return pyContext;
}

PyObject* pyCall(PyObject* pyCallable, PyObject* pyArgs)
{
    // simply call the callable with args and check for errors afterwards
    PyObject* pyResult = PyObject_CallObject(pyCallable, pyArgs);
    pyExcept(pyResult, "error during invocation of callable");

    return pyResult;
}

int test_tf(PyObject* pyContext)
{
    std::cout << "running test_tf" << std::endl;

    PyObject* pyTestFunc = PyDict_GetItemString(pyContext, "test_tf");
    PyObject* pyResult = pyCall(pyTestFunc, 0);

    long result = PyLong_AsLong(pyResult);

    Py_DECREF(pyResult);
    Py_DECREF(pyTestFunc);

    if (result == 1)
    {
        std::cout << "test_tf succeeded" << std::endl;
        return 0;
    }
    else
    {
        std::cout << "test_tf failed" << std::endl;
        std::cout << "return value: " << result << std::endl;
        return 1;
    }
}

int test_tf2(PyObject* pyContext)
{
    std::cout << "running test_tf2" << std::endl;

    PyObject* pyTestFunc = PyDict_GetItemString(pyContext, "test_tf2");
    PyObject* pyResult = pyCall(pyTestFunc, 0);

    double result = PyFloat_AsDouble(pyResult);

    Py_DECREF(pyResult);
    Py_DECREF(pyTestFunc);

    if (result == 46.)
    {
        std::cout << "test_tf succeeded" << std::endl;
        return 0;
    }
    else
    {
        std::cout << "test_tf failed" << std::endl;
        std::cout << "return value: " << result << std::endl;
        return 1;
    }
}

int test_np(PyObject* pyContext)
{
    std::cout << "running test_np" << std::endl;

    PyObject* pyTestFunc = PyDict_GetItemString(pyContext, "test_np");
    PyObject* pyResult = pyCall(pyTestFunc, 0);

    long result = PyLong_AsLong(pyResult);

    Py_DECREF(pyResult);
    Py_DECREF(pyTestFunc);

    if (result == 1)
    {
        std::cout << "test_np succeeded" << std::endl;
        return 0;
    }
    else
    {
        std::cout << "test_np failed" << std::endl;
        std::cout << "return value: " << result << std::endl;
        return 1;
    }
}

int main(int argc, char* argv[])
{
    // parse arguments
    std::string mode = argc != 2 ? "tf" : std::string(argv[1]);

    // get the test function
    int (*test)(PyObject*) = 0;
    if (mode == "tf")
    {
        test = test_tf;
    }
    else if (mode == "tf2")
    {
        test = test_tf2;
    }
    else if (mode == "np")
    {
        test = test_np;
    }
    else
    {
        throw std::runtime_error("unknown mode: " + mode);
    }

    // setup python
    pyInitialize();

    // update the python path to find tensorflow
    PyObject* pyContext = pyInitContext();
    PyObject* pySetupFunc = PyDict_GetItemString(pyContext, "insert_path");
    std::string cmsswBase = std::string(getenv("CMSSW_BASE"));
    std::string pythonPath = cmsswBase + "/python/TensorflowTest/TensorflowTest";
    PyObject* pyArgs = PyTuple_New(1);
    PyTuple_SetItem(pyArgs, 0, PyString_FromString(pythonPath.c_str()));
    pyCall(pySetupFunc, pyArgs);
    Py_DECREF(pyArgs);
    Py_DECREF(pySetupFunc);

    // run the test
    int result = test(pyContext);

    // finalize python
    Py_DECREF(pyContext);
    pyFinalize();

    return result;
}
